# -*- coding: utf-8 -*-
from io import BytesIO
from utils_routes import *
import random


class Coordinate():
    x = ""
    y = ""


class RoullteInterval():
    from_distance = 0.0
    at_distance = 0.0


list_of_sums = []


class RoundRun():
    time = 0
    great_route = []
    sum_all = 0
    sum_great_route = 0


def loop_cross_over(dictNewsGenes): #Passo 5: Chamar método de crossover para todos os novos genes/listas
    dictNewGenesCrossover = {  }

    first_rand = 0
    second_rand = 0

    com_anterior = False
    i = 0
    list_returned_cross_over = []
    for k, v in dictNewsGenes.items():
        if com_anterior: # Atual é a lista dois. No else é o inverso
            list_dois = v
            list_um = getValueItemDictAtIndex(dictNewsGenes, i-1)

            list_returned_cross_over = runCrossOverBetwenTwoPaths(list_dois, list_um, first_rand, second_rand)

            dictNewGenesCrossover.__setitem__(i, list_returned_cross_over)

            com_anterior = False
        else:
            first_rand = random.randint(0, 49)

            while True:
                second_rand = random.randint(0, 49)

                if second_rand == first_rand:
                    continue
                else:
                    break

            list_um = v
            list_dois = getValueItemDictAtIndex(dictNewsGenes, i+1)

            list_returned_cross_over = runCrossOverBetwenTwoPaths(list_um, list_dois, first_rand, second_rand)

            com_anterior = True

            dictNewGenesCrossover.__setitem__(i, list_returned_cross_over)

        i += 1

    return dictNewGenesCrossover


def runCrossOverBetwenTwoPaths(listOne, listTwo, first_rand, second_rand): #Passo 5.1: Fazer o crossover entre os caminhos selecionados e retornar um caminho com o crossover já feito
    new_list_one = []
    # Colocar o que fica no intervalo dentro da lista de item no intervalo. Tem que verificar quem é o maior e quem é o menor

    # pegar o intervalo dos selecionados:
    list_in_cross_interval = []
    if first_rand < second_rand: # considerando primeiro random menor para inicio do intervalo
        i = 0
        for item in listOne:
            if i >= first_rand and i < second_rand:
                list_in_cross_interval.append(item.x)
                new_list_one.append(listOne[i])
            else:
                new_list_one.append(None)
            i += 1
    else: # considerando segundo random menor para inicio do intervalo
        i = 0
        for item in listOne:
            if i >= second_rand and i < first_rand:
                list_in_cross_interval.append(item.x)
                new_list_one.append(listOne[i])
            else:
                new_list_one.append(None)
            i += 1

    # Para formar o 1 percorre o 2, depois verificando item a item se não está nos selecionados do 1, se não estiver coloca no novo a ser criado:
    # Começa a partir do random maior, quando chegar no fim do cromosso começa do index 0 da lista

    reference_second = 0

    if first_rand > second_rand:
        reference_second = first_rand
    else:
        reference_second = second_rand

    j = reference_second
    j_filter = reference_second
    while (j < len(listTwo)):

        if j_filter == len(new_list_one):
            j_filter = 0

        i_two = listTwo[j_filter]

        if (i_two.x not in list_in_cross_interval) and (new_list_one[j] is None):
            new_list_one.__setitem__(j, i_two)
            j += 1

        j_filter += 1

    z = 0
    z_filter = 0
    while (z < reference_second):

        i_two = listTwo[z_filter]

        if (i_two.x not in list_in_cross_interval) and (new_list_one[z] is None):
            new_list_one.__setitem__(z, i_two)
            z += 1

        if new_list_one.count(None) == 0:
            break

        z_filter += 1

    new_list_one = getListMutation(new_list_one)

    return new_list_one


def getNewsCoordinatesAfterRoullete(dictCoordRoutes, dictPercents): # Passo 4: Giro da roleta. De acordo com um número randomizado de 0 à 1. Selecionar caminho de acordo com sua porcentagem de otimização calculado no passo 3
    dictNewPathRoutes = {}
    acumulative = 0.0

    #Primeiro: Criar roleta de acordo com os valores acumulativos:
    dictRoullete = {}

    i = 0
    for k, v in dictPercents.items():
        intervalRoullete = RoullteInterval()

        intervalRoullete.from_distance = acumulative

        acumulative += v

        intervalRoullete.at_distance = acumulative

        dictRoullete.__setitem__(intervalRoullete, i)

        i += 1

    # Agora é feita a seleção. O valor de 0 à 1 tem que estar dentro de algum intervalo k adionado no dictRoullete acima.
    # De acordo com o intervalo encontrado, é retornado um index, o qual serve para add no dicionário, buscando no antigo passado por parâmetro.
    # (GIRO DA ROLETA):
    j = 0
    list_selected = []
    while j < 50: # Padrão: 50. Teste: 10
        rand_selector = random.uniform(0, 1) # de 0 à 1

        for l, w in dictRoullete.items():
            if rand_selector >= l.from_distance and rand_selector < l.at_distance:
                list_selected.append(w)

        j += 1
    # (FIM DO GIRO DA ROLETA):

    #colocar no novo dicionário de caminhos de acrodo com a lista de selecionados na roleta:
    z = 0
    while z < 50:  # Padrão: 50. Teste: 10
        indexPathSelected = list_selected[z]

        for m, x in dictCoordRoutes.items():
            if m == indexPathSelected:
                dictNewPathRoutes.__setitem__(z, x)
                z += 1
                break

    return dictNewPathRoutes


def setProbsForRoulette(dictRoutes): # Passo 3: Medir as probabilidades para colcar na roleta de seleção, colocando dentro de um dicionário referente a cada caminho de rotas.

    listSumDistances = []

    #Soma de todas as distancias para cada lista de coordenadas
    for k, v in dictRoutes.items():

        sum_distances = 0.0
        i = 1
        for coordenada in v:
            if i < len(v)-1:
                nextCoord = v[i+1]
                sum_distances += calcDistanceTwoPoints(coordenada.x, coordenada.y, nextCoord.x, nextCoord.y)
            i += 1
        listSumDistances.append(sum_distances)

    #Fim da soma de todas as distancias das listas de coordenadas

    # print(len(listSumDistances))
    # print(listSumDistances)

    global list_of_sums
    list_of_sums = listSumDistances

    # Pegar distancia por distancia e dividir pelo total da soma. De acordo com o index:
    #Dividir item por item da lista de somas de deistancias pelo total da soma de todas:
    dictPercents = {

    }
    j = 0
    sum_all_of_sums = getSumAtSumDistances(listSumDistances)
    # mmc = getMMCSumDistances(listSumDistances)
    for sum_i in listSumDistances:
        # percent = float(getPercentInverse(mmc, listSumDistances, sum_i, sum_all_of_sums)) # Recebe: MMC, soma das distâncas de uma rota e a população total
        percent = float(sum_i / sum_all_of_sums)
        dictPercents.__setitem__(j, percent)
        j += 1

    return dictPercents


def getRandomCoordinatorsFromFiles(): # Passo 1: Obter Rotas aletórias de 50 caminhos aleatórios;
    file_coordsx = open("coordenadasx.dat", "r")
    file_coordsy = open("coordenadasy.dat", "r")

    contentsx = file_coordsx.read()
    contentsy = file_coordsy.read()

    listx = contentsx.split("\n")
    listy = contentsy.split("\n")

    dictCoordsSorted = {

    }

    i = 0
    while i < 50: # deixar padrão 50 . Teste: 10

        j = 0
        list_coordinates = []
        reapeted_coord = []
        while j < 100: #Padrão: 100. Teste: 5
            random_coord = random.randint(0, 99)

            if random_coord not in reapeted_coord:
                reapeted_coord.append(random_coord)
            else:
                continue

            x_rand = listx[random_coord]
            y_rand = listy[random_coord]

            coordinate = Coordinate()
            coordinate.x = x_rand
            coordinate.y = y_rand

            list_coordinates.append(coordinate)

            j += 1

        dictCoordsSorted.__setitem__(i, list_coordinates)

        i += 1

    return dictCoordsSorted


def getSumAllAtPaths(dict): # Primeiro calcular todas as distaências, depois somar todas e retornar
    sum = 0.0

    listSumDistances = []
    # Soma de todas as distancias para cada lista de coordenadas

    for k, v in dict.items():

        sum_distances = 0.0
        i = 1
        for coordenada in v:
            if i < len(v) - 1:
                nextCoord = v[i + 1]
                sum_distances += calcDistanceTwoPoints(coordenada.x, coordenada.y, nextCoord.x, nextCoord.y)
            i += 1
        listSumDistances.append(sum_distances)

    for s in listSumDistances:
        sum += s

    return sum


def getGreatRoutes(dict):
    great_route = []

    menor = 0.0

    for k, v in dict.items():

        sum_distances = 0.0
        i = 1
        for coordenada in v:
            if i < len(v) - 1:
                nextCoord = v[i + 1]
                sum_distances += calcDistanceTwoPoints(coordenada.x, coordenada.y, nextCoord.x, nextCoord.y)
            i += 1

        if sum_distances > 0:
            if menor == 0.0:
                menor = sum_distances
                great_route = v
            else:
                if sum_distances < menor:
                    menor = sum_distances
                    great_route = v

    return great_route


def createRoundRunProcess(dict, time_runed):
    time_running = RoundRun()

    time_running.time = time_runed + 1
    time_running.sum_all = getSumAllAtPaths(dict)
    time_running.great_route = getGreatRoutes(dict)
    time_running.sum_great_route = getSumDistancesRoutes(time_running.great_route)

    return time_running


def printTests(list_runned): #Falta: Taxas de cross over e mutação;

    for test in list_runned:
        print("TIME: " + str(test.time))
        print("\tTAMANHO DA POPULAÇÃO: " + str(test.sum_all))

        listCoords = []
        for i in test.great_route:
            listCoords.append("(" + str(i.x) + ", " + str(i.y) + ")")

        print("\tMELHOR ROTA: " + str(listCoords))
        print("\n")

# try:
i = 0

list_runned = []

dictRoutesInit = {}

parar = False

while i < 50: # Quantas vezes executar: 50 vezes
    if i == 0:
        dictRoutesInit = getRandomCoordinatorsFromFiles() # 1: Caminhos com rotas aleatórias

        round_process = createRoundRunProcess(dictRoutesInit, i)

        list_runned.append(round_process)

        i += 1

    dictPercentsRoutes = setProbsForRoulette(dictRoutesInit) # 2: Porcentagem de precisão de cada caminho de rotas

    dictNewRoutes = getNewsCoordinatesAfterRoullete(dictRoutesInit, dictPercentsRoutes) # 3: Seleção na roleta

    dictNewsGenes = loop_cross_over(dictNewRoutes)

    dictRoutesInit = dictNewsGenes

    round_process = createRoundRunProcess(dictRoutesInit, i)

    list_runned.append(round_process)

    i += 1


printTests(list_runned)

from fitness_graphics import generate_graphic_pdf

generate_graphic_pdf(list_runned) # gerar gráfico na pasta graphics