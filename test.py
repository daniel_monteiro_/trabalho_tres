l = [1, 1, 1]

# print(l.count(1))
# print(len(l))

import math

def isCousin(number): # Se número é primo ou não
    divs = 0
    for di in range(1, number):
        if number % di == 0:
            divs = divs + 1
            if divs > 1:
                break
    if divs > 1:
        return False
    else:
        return True


from decimal import Decimal


def getMultAtSumDistances(lista):
    mult_count = 1.0

    for i in lista:
        mult_count *= float(i)

    return mult_count


def getMMCSumDistances(list_den):
    list_divisores = []

    divisor = 2

    i_den = 0

    while i_den < len(list_den):
        if not isCousin(divisor):
            divisor += 1
            continue

        number_den = list_den[i_den]
        # den_int = number_den * 1000000000000
        den_int = int(number_den)

        j = 0
        list_plus_divisores = []
        while j < len(list_divisores):
            div_j = list_divisores[j]

            qtd_with_j = 0
            while den_int%div_j == 0:
                den_int /= div_j

                if qtd_with_j > 0:
                    list_plus_divisores.append(div_j)

                qtd_with_j += 1

            j += 1

        list_divisores += list_plus_divisores

        added = False
        while den_int > 1:
            if den_int % divisor == 0:
                den_int /= divisor

                added = True

                if int(den_int) == 1:
                    list_divisores.append(divisor)
                    divisor += 1
                    added = False

                if divisor not in list_divisores and added:
                    list_divisores.append(divisor)
                    added = False
            else:
                if added:
                    list_divisores.append(divisor)
                    added = False
                divisor += 1

        divisor = 2
        i_den += 1

    return getMultAtSumDistances(list_divisores)

# print(getMultAtSumDistances([2, 2, 7, 7, 13, 5333, 5, 541, 1091, 5209, 2, 2, 3, 223, 3, 563, 67, 79, 2, 2, 2, 3, 37, 1093, 1747, 2, 2, 3, 11, 353, 3, 3, 19, 61, 83, 263, 5, 17, 311, 5, 199, 307, 2, 2, 2, 337, 2, 1213, 2657, 109, 53, 509, 2, 1249, 71, 4723, 103, 277, 521, 5281, 5113, 41, 43, 7, 107, 863, 2, 1321, 2, 1307, 47, 683, 5171, 1723, 1783, 3]))

# print(getMMCSumDistances([45612312332, 1234567895]))

import math

# print((261979.46161226393+261231.96498656622)/2)

# print(round(0.3214351351515615, 6))