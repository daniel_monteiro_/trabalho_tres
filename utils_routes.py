# -*- coding: utf-8 -*-
import random
import math
from decimal import Decimal


def calcDistanceTwoPoints(x1, y1, x2, y2): # Passo 2: Calcular a distância entre dois ponto;
    x1 = float(x1)
    y1 = float(y1)

    x2 = float(x2)
    y2 = float(y2)

    a = (x2-x1) * (x2-x1)
    b = (y2-y1) * (y2-y1)

    d = a + b

    dResult = math.sqrt(d)

    return float(dResult)


def getSumDistancesRoutes(listRoutes):
    sum_distances = 0.0
    i = 1
    for coordenada in listRoutes:
        if i < len(listRoutes) - 1:
            nextCoord = listRoutes[i + 1]
            sum_distances += calcDistanceTwoPoints(coordenada.x, coordenada.y, nextCoord.x, nextCoord.y)
        i += 1

    return sum_distances


def getSumAtSumDistances(lista):
    sum_count = 0.0

    for i in lista:
        sum_count += i

    return sum_count


def getMultAtSumDistances(lista):
    mult_count = 1

    for i in lista:
        mult_count *= float(i)

    return Decimal(mult_count)


def printDictPercents(dictPercents):
    sum_v = 0.0
    for k, v in dictPercents.items():
        print (str(k)+ ": ")
        print(str(v))
        # if k < 5:
        #     sum_v += v

    print(sum_v)


def printDictCoords(dictCoordsSorted):
    # lista = []
    for k, v in dictCoordsSorted.items():
        print(str(k))
        # print("\t")
        # print(v)

        lista = []
        for c in v:

            # if None not in v:
            #     print("Aqui em baixo")

            if not c is None:
                lista.append("(" + str(c.x) + "," + str(c.y) + ")")
            else:
                lista.append("(" + str("None") + "," + str("None") + ")")
            # print("")
        # lista.append(v)
        print(lista)
        print("\n")
        lista.append(v)


def getValueItemDictAtIndex(dict, index):
    list_return = []

    for k, v in dict.items():
        if index == k:
            list_return = v
            break

    return list_return


def getListMutation(list):
    rand_troca_um = random.randint(0, len(list)-1)

    rand_troca_dois = 0

    while True:
        rand_troca_dois = random.randint(0, len(list) - 1)

        if rand_troca_dois == rand_troca_um:
            continue
        else:
            break

    coord1 = list[rand_troca_um]
    coord2 = list[rand_troca_dois]

    list.__setitem__(rand_troca_um, coord2)
    list.__setitem__(rand_troca_dois, coord1)

    return list


def isCousin(number): # Se número é primo ou não
    divs = 0
    for di in range(1, number):
        if number % di == 0:
            divs = divs + 1
            if divs > 1:
                break
    if divs > 1:
        return False
    else:
        return True


def getMMCSumDistances(list_den):
    list_divisores = []

    divisor = 2

    i_den = 0

    while i_den < len(list_den):
        if not isCousin(divisor):
            divisor += 1
            continue

        number_den = list_den[i_den]
        # den_int = number_den * 1000000000000
        den_int = int(number_den)

        j = 0
        list_plus_divisores = []
        while j < len(list_divisores):
            div_j = list_divisores[j]

            qtd_with_j = 0
            while den_int%div_j == 0:
                den_int /= div_j

                if qtd_with_j > 0:
                    list_plus_divisores.append(div_j)

                qtd_with_j += 1

            j += 1

        list_divisores += list_plus_divisores

        added = False
        while den_int > 1:
            if den_int % divisor == 0:
                den_int /= divisor

                added = True

                if int(den_int) == 1:
                    list_divisores.append(divisor)
                    divisor += 1
                    added = False

                if divisor not in list_divisores and added:
                    list_divisores.append(divisor)
                    added = False

                # print(str(den_int) + ": "+str(divisor))
            else:
                if added:
                    list_divisores.append(divisor)
                    added = False
                divisor += 1

        divisor = 2
        i_den += 1

    return getMultAtSumDistances(list_divisores)


def getPercentInverse(mmc, listDenominators, denominator, population):
    # 1 Descobrir soma das variáveis classificatórias(que nosso caso a soma de todas distâncias das, que chamei de x) [MMC dividido pelo denominador]

    sum_vars_class_donimators = 0
    for i in listDenominators:
        x = Decimal(mmc / Decimal(i))
        sum_vars_class_donimators += x

    # 2 Criar constante de proporcionalidade:
    k = Decimal(population) / sum_vars_class_donimators

    # Multiplicar valor da constante com o valor da variável classificatória desoberta na passo 1
    percent = k * Decimal(denominator)
    # percent /= Decimal(1000000000000)
    # return round(percent, 6)
    return percent