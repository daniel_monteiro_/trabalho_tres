# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import datetime


def generate_graphic_pdf(list_rounds):
    list_plot = []

    for r in list_rounds:
        list_plot.append(r.sum_great_route) # seta desempenho da melhor rota

    plt.plot(list_plot)
    plt.ylabel("Desempenho melhor rota")
    plt.xlabel("Rodadas de execução")

    plt.savefig('graphics/fit_' + datetime.datetime.now().strftime("%d_%m_%Y__%H_%M_%S") + '.pdf')